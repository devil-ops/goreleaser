#!/usr/bin/env bash
set -e

if [[ -z "$1" ]]; then
  echo "Usage: $0 <vault-role>" 1>&2
  exit 2
fi

# We may wanna abstract this more later
GITLAB_INST=$(echo "$CI_SERVER_HOST" | awk -F . '{print $1}')

# Role should be something like ssi-systems-project-name
ROLE=$1

export VAULT_ADDR=https://vault-mgmt.oit.duke.edu
VAULT_TOKEN=$(vault write -field=token "auth/global/${GITLAB_INST}/login" role="${ROLE}" jwt="${VAULT_ID_TOKEN}")
export VAULT_TOKEN

GPG_KEY_PATH="/tmp/signing.key"
vault kv get -field=key ssi-systems/kv/devil-ops/package-signing/gpg > "${GPG_KEY_PATH}"
NFPM_RPMS_PASSPHRASE=$(vault kv get -field=passphrase ssi-systems/kv/devil-ops/package-signing/gpg)
echo "export GPG_KEY_PATH=${GPG_KEY_PATH}"
echo "export NFPM_RPMS_PASSPHRASE=${NFPM_RPMS_PASSPHRASE}"

UPLOAD_PRODUCTIONRPM_SECRET=$(vault kv get -field=key ssi-systems/kv/devil-ops/proget/rpm)
UPLOAD_PRODUCTIONDEB_SECRET=$(vault kv get -field=key ssi-systems/kv/devil-ops/proget/deb)
SCOOP_TAP_TOKEN=$(vault kv get -field=token ssi-systems/kv/devil-ops/scoop/gitlab-token)

if [[ -z "$HOMEBREW_TAP_TOKEN" ]]; then
  HOMEBREW_TAP_TOKEN=$(vault kv get -field=token ssi-systems/kv/devil-ops/homebrew/gitlab-token)
else
  echo "HOMEBREW_TAP_TOKEN already set in the env, using that instead of the one from vault" 1>&2
fi

echo "export UPLOAD_PRODUCTIONRPM_SECRET=${UPLOAD_PRODUCTIONRPM_SECRET}"
echo "export UPLOAD_PRODUCTIONDEB_SECRET=${UPLOAD_PRODUCTIONDEB_SECRET}"
echo "export HOMEBREW_TAP_TOKEN=${HOMEBREW_TAP_TOKEN}"
echo "export SCOOP_TAP_TOKEN=${SCOOP_TAP_TOKEN}"
echo "export VAULT_ADDR=${VAULT_ADDR}"
echo "export VAULT_TOKEN=${VAULT_TOKEN}"
echo "export GITLAB_TOKEN=${CI_JOB_TOKEN}"

