#!/bin/bash
set -e

if [[ -z "$1" ]]; then
   GRF=".goreleaser.yaml"
else
   GRF=$1
fi

if [ -z "${VAULT_ID_TOKEN}" ]; then
	echo "Must set VAULT_ID_TOKEN"
	exit 2
fi

## Ensure that a couple sections in here are present
echo "Ensuring that the first signer is 'cosign'"
yq -e '.signs.0.cmd | (.=="cosign")' "$GRF" >/dev/null

echo "Ensuring that the first env var is the VAULT_ADDR"
yq -e '.env.0 | (.=="VAULT_ADDR={{ .Env.VAULT_ADDR }}")' "$GRF" >/dev/null

echo "Ensure we passing --tlog-upload=false in to the run"
yq -e '.signs.0.args.1| (. == "--tlog-upload=false")' "$GRF" >/dev/null

echo "Ensure we are using output-signature is present in args"
yq -e '.signs.0.args.3 | (. == "*output-sig*")' "$GRF" >/dev/null

echo "Ensure we are using the package registry instead of gitlab uploads"
yq -e '.gitlab_urls.use_package_registry | (.==true)' "$GRF" >/dev/null

echo "Ensure we are using the CI_JOB_TOKEN instead of a normal GITLAB_TOKEN"
yq -e '.gitlab_urls.use_job_token | (.==true)' "$GRF" >/dev/null

echo "Ensure if we are using rpm packaging that we have the right signing statement"
yq -e '.nfpms | map(select(.formats[] == "rpm"))' "$GRF" >/dev/null && yq -e '.nfpms | map(select(.formats[] == "rpm"))| .[].rpm.signature.key_file | (. == "*Env.GPG_KEY_PATH*")' "$GRF" >/dev/null

echo "Ensure that we are generating sboms for the archive"
yq -e '.sboms[0].artifacts | ( .=="archive") ' "$GRF" >/dev/null

goreleaser healthcheck

echo "${GRF} is good! 👍"
