FROM goreleaser/goreleaser:v2.5.0

## Unsure how to do this in a way that passes the linter...🤔
# hadolint ignore=DL3022
#COPY --from=anchore/syft:v0.46.1@sha256:2edcb655d3eada83dce419f55811254bfd83161940bcd7cc3bd98f0609d0c8eb /syft /usr/local/bin/syft
COPY --from=anchore/syft:latest /syft /usr/local/bin/syft
# hadolint ignore=DL3022
COPY --from=hashicorp/vault:latest /bin/vault /usr/local/bin/vault
# hadolint ignore=DL3022
COPY --from=mikefarah/yq:4.27.2@sha256:23fdacd7da0a4f7b2bae9583e5ab610227a978c38f4b1462f2f1053af6a9573c /usr/bin/yq /usr/local/bin/yq
# hadolint ignore=DL3022
COPY --from=golangci/golangci-lint:latest /usr/bin/golangci-lint /usr/local/bin/golangci-lint

# hadolint ignore=DL3018
RUN echo https://dl-cdn.alpinelinux.org/alpine/edge/testing/ >> /etc/apk/repositories && \
    apk update && \
    apk --no-cache add \
      jq \
      tar \
      ko && \
    go install golang.org/x/vuln/cmd/govulncheck@latest && \
    go install github.com/boumenot/gocover-cobertura@latest

COPY scripts /scripts
COPY golangci /golangci

ENTRYPOINT [""]
